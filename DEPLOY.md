# Deploying the oracle

## Prerequisites

* nginx
* the Rust toolchain
* `bitcoind` running on the same machine

optional:

* tor

## Compile and install

    ```shell
    cargo build --release -p txood
    sudo cp target/release/txood /usr/local/bin/
    ```

## Setup with local webserver

Ensure that `bitcoind` is running on the same machine and user (e.g. `bitcoind --daemon --testnet`).

### Configure web directory

    ```shell
    sudo mkdir /var/www/html/txoo
    sudo chown $USER /var/www/html/txoo
    ```

### Optional - allow directory listing

Apply the following configuration to nginx:

    ```
        location /txoo {
            autoindex on;
        }
    ```

### Running manually

    ```shell
    txood --network testnet
    ```

Note: if your `bitcoind` doesn't use cookie authentication, you can specify the RPC URL with `-r`,
e.g. `-r http://user:pass@localhost:18332/`.  You also have to change `txood-testnet.service` if you are using systemd.

### Sync the web directory manually

    ```shell
    ./contrib/sync-txoo-local
    ```

## Configure clients

The oracle public key can be obtained at `http://localhost/txoo/testnet/config`.

## Configure with `systemd`

Instead of running manually as described above, you can use `systemd` to run `bitcoind` and `txood` as a service:

    ```shell
    sudo cp contrib/{bitcoind-testnet,txood-testnet}.service /etc/systemd/system/
    sudo systemctl daemon-reload
    sudo systemctl enable txood-testnet
    sudo systemctl start txood-testnet
    sudo systemctl enable bitcoind-testnet
    sudo systemctl start bitcoind-testnet
    systemctl status txood-testnet bitcoind-testnet
    journalctl -u txood-testnet
    ```

Syncing the web directory can be done using a cron job with `contrib/sync-txoo-local`:

    ```shell
    crontab -e
    ```

    ```
    * * * * * cd /home/bitcoin/txoo && ./contrib/sync-txoo-local
    ```

## Setup with Tor

TBD
